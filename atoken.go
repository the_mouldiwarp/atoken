package main

import (
	"fmt"

	"github.com/howeyc/gopass"
)

import b64 "encoding/base64"

type creds struct {
	uname, pword, authToken string
}

type credentials interface {
	getUsername() string
	getPassword() string
	genAuthToken() string
	getCreds() string
}

func (c creds) genAuthToken() string {
	str := fmt.Sprintf("%s:%s", c.uname, c.pword)
	return string(b64.StdEncoding.EncodeToString([]byte(str)))
}

func (c creds) getUsername() string {
	fmt.Println("Userername: ")
	u, e := gopass.GetPasswdMasked()
	if e != nil {
		panic(e)
	}
	return string(u[:])
}

func (c creds) getPassword() string {
	fmt.Println("Password: ")
	p, e := gopass.GetPasswdMasked()
	if e != nil {
		panic(e)
	}
	return string(p[:])
}

func (c *creds) getCreds() {
	c.uname = c.getUsername()
	c.pword = c.getPassword()
	c.authToken = c.genAuthToken()
}

func byteSliceToString(s []byte) string {
	return string(s[:])
}

func main() {
	c := creds{"", "", ""}
	s := &c
	s.getCreds()
	out := fmt.Sprintf("Auth token: %s", s.authToken)
	fmt.Println(out)
}
